<?php
$dataset = array(
    array(29,23,4,11,20,12,19,26,3,5,2,6,22,18,27),
    array(5,23,19,20,10,27,3,30,14,28,15,25,6,24,4),
    array(11,19,9,22,3,10,20,1,28,25,16,21,24,2,30),
    array(1,17,6,11,22,16,9,5,19,10,25,15,23,3,13),
    array(12,22,27,17,1,18,8,21,6,29,20,7,28,23,5),
    array(16,28,7,17,21,8,1,13,9,15,25,14,30,10,24),
    array(8,12,11,26,17,2,9,29,18,13,21,1,16,22,7),
    array(29,6,7,26,13,4,12,5,23,8,27,14,15,18,17),
    array(2,8,14,30,4,18,29,27,28,7,26,20,12,24,21),
    array(11,9,24,25,2,16,3,10,19,15,4,13,26,30,14));

// Check the intented action of the user

if (isset($_REQUEST['action']))
{
	if ($_REQUEST['action'] == 'eval')
	{
		$action = 'eval';
		if (isset($_REQUEST['pg']) 
      && isset($_REQUEST['dset'])
			&& isset($_REQUEST['res']))
		{
			$pg = $_REQUEST['pg'];
			$dset = $_REQUEST['dset'];
			$res = $_REQUEST['res'];
      $results = split(",",$res);
      if ($_REQUEST['choice'] == "CG Image")
        $results[$dataset[$dset][$pg]] = 'C';
      elseif  ($_REQUEST['choice'] == "Photograph")
        $results[$dataset[$dset][$pg]] = 'R';
      $res = join(",",$results);
      $pg++;

      if ($pg == 15)
      {
        $action = "thanks";
        $fp = fopen('qdat/results.txt', 'a');
        fwrite($fp,$res . "\n");
        fclose($fp);
      }
      else
      {
        $picnum = $dataset[$dset][$pg];
      }
		}
		else
		{
			$pg = 0;
			$dset = rand(0,9);
			$res = str_repeat("0,",31); // 1-30 data points for each pic, Pos 0 is just a placeholder
      $picnum = $dataset[$dset][$pg];
		}
	}
	elseif ($_REQUEST['action'] == 'contact')
	{
    if (isset($_REQUEST['text']))
    {
        $fp = fopen('qdat/messages.txt', 'a');
        fwrite($fp,"Name: " . htmlspecialchars($_REQUEST['name']) . "\n");
        fwrite($fp,"Email: " . htmlspecialchars($_REQUEST['email']) . "\n");
        fwrite($fp,"Subject: " . htmlspecialchars($_REQUEST['subject']) . "\n");
        fwrite($fp,"Message:\n>>>" . htmlspecialchars($_REQUEST['text']) . "\n");
        fwrite($fp,"===================================\n");
        fclose($fp);
        $action = "msgthanks";    
    }  
    else
		    $action = 'contact';
	}
	elseif ($_REQUEST['action'] == 'admin')
	{
      $action = 'admin';
    	$activity = 'login';
      if (isset($_REQUEST['pass']) &&
        	($_REQUEST['pass'] == '@dmin4StuPics!!!') &&
	        isset($_REQUEST['activity']))
      {
        $pass = $_REQUEST['pass'];
        if (isset($_REQUEST['activity']))
           $activity = $_REQUEST['activity'];  
        else
           $activity = "Show menu";
      } 
	}
	else
		$action = 'home';
}	
else
	$action = 'home';

//======================================= functions
// Identify the current action
function check($act)
{   global $action;
	if ($act == $action)
      return("<li class='active'>");
    else
      return("<li>");
}

// ===============================================

function homePage()
{
    print <<<EOT
      <table style="border-spacing: 10px; border-collapse: separate;">
      <tbody>
      <tr>
        <td><h2>Evaluation of the PYU StuPic Processor</h2></td>
        <td><h2>แบบประเมินคุณภาพ "ภาพถ่ายนักศึกษา"</h2></td>
    </tr>
    <tr>
        <td>
        <p>This is a questionnaire to assess the quality of
        the output from a program that converts pictures of university
        applicants wearing common dress into photographic images of 
        students in uniform suitable for a student directory. These 
        computer-generated pictures are mere placeholders in the 
        student directory until they can be replaced by student ID
        photographs. You are invited to provide an
        assessment of the quality of the images produce. 
        Assessment will be done by asking volunteers like you to review
        a set of 15 images which comprises a mixture of computer-generated
        images and actual photographs.
        However, in this study, the eyes in these photographs were blurred
        in accordance to privacy rules associated with this kind of research.</p>
    </td>
    <td>
    <p>แบบประเมินนี้ จัดทำขึ้นเพื่อประเมินคุณภาพของผลลัพธ์จากงานวิจัย "การสร้างรูปประจำตัวนักศึกษาจากภาพถ่ายของนักศึกษา
ในชุดลำลอง" โดยโปรแกรมที่พัฒนาจะทำการแทนที่ชุดนักศึกษาลงไปแทนที่ชุดลำลองของนักศึกษาในภาพ  แบบประเมินนี้จะมีจำนวนรูปภาพสำหรับการทดสอบทั้งสิ้น 15 ภาพ ซึ่งคละกันระหว่างรูปภาพที่นักศึกษาแต่งกายด้วยชุดนักศึกษาจริง กับรูปภาพที่เกิดจากการแทนที่ชุดนักศึกษาด้วยแอปพลิเคชันที่พัฒนาในงานวิจัย อย่างไรก็ตาม ภาพที่ปรากฏจะถูกทำให้เบลอบริเวณดวงตาเพื่อป้องกันความเป็นส่วนตัวของบุคคลในภาพ</p>
</td>
</tr>
<td>
<p>For each image in the set, you will be asked to determine if the image is
 the result of computer adaption of a student application photo
  or an actual photograph of a student in university uniform.</p>
</td><td>
<p>โดยให้ผู้ประเมินทำการตัดสินว่าภาพใดเป็นภาพที่นักศึกษาแต่งกายด้วยชุดนักศึกษาจริง หรือเป็นภาพที่เกิดจากการแทนที่ชุดนักศึกษาด้วยแอปพลิเคชัน</p>
</td></tr>
<tr><td>
<p>Your participation in this questionnaire is valued and appreciated.</p>
</td><td>
<p>ขอขอบคุณในการมีส่วนร่วมสำหรับการประเมินผลงานวิจัยชิ้นนี้</p>
</td></tr>
<tr>
  <td align="center">The Research Team</td>
  <td align="center">ทีมวิจัย</td>
</tr>
</tbody>
</table>  
    <h2 align="center">
    <a href="?action=eval"><span class="label label-primary">START</span></a>
    <a href="?action=contact"><span class="label label-info">CONTACT US</span></a>    
    </h2>
EOT;
}


function thanks()
{
    print <<<EOT
    <center>
 <h2>Your responses have been recorded.<br>
 คำตอบได้ถูกบันทึกเรียบร้อยแล้ว</h2>
 <img src="pics/thanks.png">
    </center>
EOT;
}

function msgthanks()
{
    print <<<EOT
    <center>
 <h2>Your messages has been sent.<br>
 ข้อความของคุณได้ถูกส่งให้กับทีมวิจัยเป็นที่เรียบร้อยแล้ว</h2>
    </center>
EOT;
}


// ===============================================

function login()
{
    print <<<EOT
 <center>
 <h2>Login</h2>
 <form action="?action=admin" method="post">
 <table>
 <tr>
 <td>Pass phrase</td>
 </tr>
 <tr>
 <td align="center">
   <input type="password" name="pass" value="">
 </td>
 </tr>
 <tr>
 <td align="center">
   <input type="submit" name="submit" value="Enter">
 </td>
 </tr>
 </table>
 <input type=hidden name=activity value="Show menu">
 </form>
 </center>
EOT;
}

// ===============================================

function menu()
{ global $pass;
  print <<<EOT
<center>
  <h2>Menu</h2>
  <form action="?action=admin" method=post>
  <p><input type=submit name=activity value="Display messages"></p>
  <p><input type=submit name=activity value="Display results"></p>
  <input type=hidden name="pass" value="$pass">
  <input type=hidden name="action" value="admin">
  </form>
</center>
EOT;
}

// ===============================================

function displaymsg()
{
  global $pass;

  print <<<EOT
<center>
  <h2>Messages on file</h2>
  <form action="?" method="post">
  <textarea cols=40 rows=10>
EOT;

  readfile("qdat/messages.txt");

  print <<<EOT
  </textarea>
  <p><input type=submit name=activity value="Show menu"></p>
  <input type=hidden name=action value="admin">
  <input type=hidden name=pass value="$pass"> 
  </form>
</center>
EOT;
}

// ===============================================

function displayresults()
{
  global $pass;

  print <<<EOT
<center>
  <h2>Questionnaire results</h2>
    <form action="?" method="post"> 
  <textarea cols=65 rows=10>
EOT;

  readfile("qdat/results.txt");

  print <<<EOT
  </textarea>
  <input type=hidden name=action value="admin">
  <input type=hidden name=pass value="$pass"> 
  <p><input type=submit name=activity value="Show menu"></p>
  </form>
</center>
EOT;
}

// ===============================================

function clearmsg()
{
  print <<<EOT
<center>
  <h2>Messages have been deleted</h2>    
  <form action="?action=admin" 
  <p><input type=submit name=activity value="Menu"></p>
  </form>
</center>
EOT;
}

// ===============================================

function contact()
{
    print <<<EOT
<center>
  <h2>Contacting the Research Team<br>
           ติดต่อทีมวิจัย</h2></center>
  <form method="post" action="?action=contact">


  <p>Please fill in the contact form:</p>
       
  <div style="width:50%;min-width:100px;">

    <div style="float:left;min-width:100px;">Name:</div>
    <div style="float:left;"><input type=text name=name
          placeholder="your name" required maxlength=100 size=30>
    </div>
    <div style="clear:both;"></div>

    <div style="float:left;min-width:100px;">Email address:</div>
    <div style="float:left;"><input type=text name=email 
          placeholder="your email address" required 
                maxlength=100 size=30></div>
    <div style="clear:both;"></div>        
          
    <div style="float:left;min-width:100px;">Topic:</div>
    <div style="float:left;"><input type=text name=subject 
          placeholder="subject of your message" required 
              maxlength=100 size=30></div>
    <div style="clear:both;"></div>

    <div style="float:left;min-width:100px;">Message:</div>
    <div style="float:left;">
        <textarea name=text rows=6 cols=30 maxlength=300
                placeholder="Message" required></textarea>
    </div>
    <div style="clear:both;"></div>

    <div>
EOT;

  $previous = "javascript:history.go(-1)";
  if(isset($_SERVER['HTTP_REFERER'])) 
  {
      $previous = $_SERVER['HTTP_REFERER'];
  }

  print <<<EOT
    <button type="button" onclick="<?= $previous ?>">Back</button>
    <input type=submit name=submit value="Send">
    <input type=reset name=submit value="Cancel">
      </div>  
  </form>
</center>
EOT;
}

// ===============================================

function evalpic()
{
  global $pg;
  global $dset;
  global $res;
  global $picnum;

  print <<<EOT
<form method=post action="?">
<center> 
  <h2>Image Evaluation<br>
  ประเมินผลภาพ</h2>
  <img src="pics/pict$picnum.jpg" alt="P1">
  <h3>Please choose one.<br>กรุณาเลือก</h3>
  <div>
  <div style="float:left;padding:10px;min-width:200px; width:45%;">
    <p>
        <input type="submit" name="choice" value="Photograph">
    </p>
    <p style="text-align:left;">
      This is a photograph of a student in university uniform.<br>
    ภาพที่นักศึกษาแต่งกายด้วยชุดนักศึกษาจริง</p>
  </div>
  <div style="float:left;padding:10px;min-width:200px; width:45%;">
    <p>
        <input type="submit" name="choice" value="CG Image">
    </p>
    <p style="text-align:left;">
      This is a computer-generated image from an applicant wearing 
       common clothes.<br>
      ภาพที่เกิดจากการแทนที่ชุดนักศึกษาด้วยแอปพลิเคชัน</p>
  </div> 
    <div style="clear:both;"></div>
  </div>

</center>
<input type="hidden" name="action" value="eval">
<input type="hidden" name="pg" value="$pg">
<input type="hidden" name="dset" value="$dset">
<input type="hidden" name="res" value="$res">

</form>
EOT;
}
// ===============================================================
?>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" 	href="./bootstrap-3.3.6/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="./bootstrap-3.3.6/css/bootstrap-theme.min.css">
<!-- Latest JavaScript -->
<script src="./bootstrap-3.3.6/js/bootstrap.min.js"></script>
<title>Evaluation of Student Pictures</title>
<style>
body {
  padding-top: 70px;
  padding-bottom: 30px;
}

.theme-dropdown .dropdown-menu {
  position: static;
  display: block;
  margin-bottom: 20px;
}

.theme-showcase > p > .btn {
  margin: 5px 0;
}

.theme-showcase .navbar .container {
  width: auto;
}

body {
  background-color: #99ccff;
}

.box {
  margin-left: auto; 
  margin-right: auto;
}
</style>
</head>

<body role="document">
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">PYU StuPics Processing:</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?= check("home") ?><a href="?action=home">Home</a></li>
            <?= check("eval") ?><a href="?action=eval">Evaluation</a></li>
            <?= check('contact') ?><a href="?action=contact">Contact</a></li>
            <?= check('admin') ?><a href="?action=admin">Admin</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container theme-showcase" role="main">

      <div class="jumbotron">
      	<img src="./pics/pyulogo.png" alt="Payap University logo">

    
    <?php
    if ($action == "contact")
    {
        contact();
    }
    elseif ($action == "eval")  
        evalpic();
    elseif ($action == "admin")
    {
      if ($activity == "Show menu")
          menu();
      elseif ($activity == "Display messages")
          displaymsg();
      elseif ($activity == "Display results")
          displayresults();
      elseif ($activity == "Clear messages")
          clearmsg();
      else
         login();
    }
    elseif ($action == "msgthanks")
        msgthanks(); 
    elseif ($action == "thanks")
        thanks();     
    else
    { 
      homePage();
    }
?>
    </div>
  </div>

  <?php
//   print_r($_REQUEST); 
//  <?php print($res);  
  ?>
</body>
</html>
